# Thomas Hahn, 2020
#
# File: Convenience module file
# Description: Set custom compiler options.
#
# CUSTOM_OPTION_NO_WARN: do not set "-Wall -Werror"
# CUSTOM_OPTION_WERROR: do set "-Werror"
# CUSTOM_OPTION_PEDANTIC: do set "-pedantic"
# CUSTOM_OPTION_NO_LTO: do not use link time optimization

# warnings
if(NOT CUSTOM_OPTION_NO_WARN)
    add_compile_options(-Wall -Wextra)
endif()

if(CUSTOM_OPTION_WERROR)
    add_compile_options(-Werror)
endif()

if(CUSTOM_OPTION_PEDANTIC)
    add_compile_options(-pedantic)
endif()

# LTO
if(NOT CUSTOM_OPTION_NO_LTO)
    if(CMAKE_CXX_COMPILER_ID STREQUAL "Intel")
        add_compile_options(-ipo)
        add_link_options(-ipo)
    else()
        add_compile_options(-flto)
        add_link_options(-flto)
    endif()
endif()
