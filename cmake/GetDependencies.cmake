# Thomas Hahn, 2020
#
# File: Convenience module file
# Description: Helper functions to declare, fetch or find various dependencies

# fetch_from_git(): Fetches a repository from git and adds it via
# add_subdirectory()
#
# CONTENT_NAME: name of the fetched content (is arbitrary)
# GIT_TAG: git tag to fetch content
# GIT_REPOSITORY: git repository to fetch content
# SOURCE_DIRECTORY: source directory from which the content should be
# fetched instead of the git repository
function(fetch_from_git)
    # define keyword arguments
    set(prefix FFG)
    set(noValues)
    set(singleValues CONTENT_NAME GIT_TAG GIT_REPOSITORY SOURCE_DIRECTORY)
    set(multiValues)

    # process the arguments passed in
    include(CMakeParseArguments)
    cmake_parse_arguments(${prefix}
        "${noValues}"
        "${singleValues}"
        "${multiValues}"
        ${ARGN})

    # check the arguments
    if(NOT FFG_CONTENT_NAME)
        message(FATAL_ERROR "You must specify a CONTENT_NAME in the function
            fetch_from_git.")
    endif()
    if(NOT (FFG_GIT_TAG AND FFG_GIT_REPOSITORY) AND NOT FFG_SOURCE_DIRECTORY)
        message(FATAL_ERROR "You must specify either a GIT_TAG and GIT_REPOSITORY
            or a SOURCE_DIRECTORY in the function fetch_from_git.")
    endif()

    # fetch from git and add subdirectories
    message(STATUS "---------------------")
    message(STATUS "Fetching ${FFG_CONTENT_NAME} ...")
    include(FetchContent)
    FetchContent_Declare(${FFG_CONTENT_NAME}
        GIT_REPOSITORY ${FFG_GIT_REPOSITORY}
        GIT_TAG ${FFG_GIT_TAG})
    string(TOUPPER ${FFG_CONTENT_NAME} UC_CONTENT_NAME)
    string(TOLOWER ${FFG_CONTENT_NAME} LC_CONTENT_NAME)
    set(FETCHCONTENT_SOURCE_DIR_${UC_CONTENT_NAME} ${FFG_SOURCE_DIRECTORY})
    FetchContent_GetProperties(${FFG_CONTENT_NAME})
    if(NOT ${LC_CONTENT_NAME}_POPULATED)
        FetchContent_Populate(${FFG_CONTENT_NAME})
        add_subdirectory(${${LC_CONTENT_NAME}_SOURCE_DIR}
            ${${LC_CONTENT_NAME}_BINARY_DIR})
    endif()
    message(STATUS "Source directory: ${${LC_CONTENT_NAME}_SOURCE_DIR}")
    message(STATUS "Binary directory: ${${LC_CONTENT_NAME}_BINARY_DIR}")
endfunction()

# fetch_boost(): Adds boost as an external project, builds the header files
#                and uses find_package() to make the target Boost::boost global
# GIT_TAG: git tag to download
# GIT_REPOSITORY: git repository to download from
# SOURCE_DIRECTORY: source directory from which the content should be fetched
# instead of the git repository
function(fetch_boost)
    # define keyword arguments
    set(prefix FB)
    set(noValues INSTALL_BOOST)
    set(singleValues GIT_TAG GIT_REPOSITORY SOURCE_DIRECTORY)
    set(multiValues)

    # process the arguments passed in
    include(CMakeParseArguments)
    cmake_parse_arguments(${prefix}
        "${noValues}"
        "${singleValues}"
        "${multiValues}"
        ${ARGN})

    # check the arguments
    if(NOT (FB_GIT_TAG AND FB_GIT_REPOSITORY) AND NOT FB_SOURCE_DIRECTORY)
        message(FATAL_ERROR "You must specify either a GIT_TAG and
            GIT_REPOSITORY or a SOURCE_DIR in the function fetch_boost.")
    endif()

    # add external project, download, build and make targets global
    message(STATUS "---------------------")
    message(STATUS "Fetching boost ...")
    include(ExternalProject)
    ExternalProject_Add(boost
        SOURCE_DIR ${FB_SOURCE_DIRECTORY}
        GIT_REPOSITORY $<$<NOT:$<BOOL:${FB_SOURCE_DIRECTORY}>>:${FB_GIT_REPOSITORY}>
        GIT_TAG $<$<NOT:$<BOOL:${FB_SOURCE_DIRECTORY}>>:${FB_GIT_TAG}>
        CONFIGURE_COMMAND ""
        BUILD_COMMAND <SOURCE_DIR>/bootstrap.sh
              COMMAND <BINARY_DIR>/b2 headers
        BUILD_IN_SOURCE ON
        INSTALL_COMMAND "" 
    )
    ExternalProject_Get_Property(boost SOURCE_DIR)
    set(BOOST_ROOT "${SOURCE_DIR}" CACHE PATH "" FORCE)
    find_package(Boost REQUIRED)
    set_target_properties(Boost::boost PROPERTIES IMPORTED_GLOBAL TRUE)
endfunction()
