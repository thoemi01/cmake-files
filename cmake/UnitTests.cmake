# Thomas Hahn, 2020
#
# File: Convenience module file
# Description: Helper function to add tests

# add_unittest(): Adds a unittest for a given sources and libs
# NAME: name of the test
# SOURCES: source files
# LIBS: libraries for linking
function(add_unittest)
    # define keyword arguments
    set(prefix AU)
    set(noValues)
    set(singleValues NAME)
    set(multiValues SOURCES LIBS)

    # process the arguments passed in
    include(CMakeParseArguments)
    cmake_parse_arguments(${prefix}
        "${noValues}"
        "${singleValues}"
        "${multiValues}"
        ${ARGN})

    # check the arguments
    if(NOT AU_NAME)
        message(FATAL_ERROR "You must specify a NAME in the function add_unittest.")
    endif()
    if(NOT AU_SOURCES)
        message(FATAL_ERROR "You must specify SOURCES in the function add_unittest.")
    endif()

    # create executable
    add_executable(${AU_NAME} ${AU_SOURCES})
    target_link_libraries(${AU_NAME} ${AU_LIBS})
    
    # add test
    add_test(NAME ${AU_NAME} COMMAND ${AU_NAME})
endfunction()
